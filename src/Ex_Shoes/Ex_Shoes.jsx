import React, { Component } from "react";
import { dataShoes } from "./data_shoes.js";
import ItemShoes from "./ItemShoes";
import GioHang from "./GioHang";
export default class Ex_Shoes extends Component {
  state = {
    shoes: dataShoes,
    gioHang: [],
  };

  renderShoes = () => {
    return this.state.shoes.map((item, index) => {
      return (
        <ItemShoes
          handleAddToCard={this.handleAddToCard}
          data={item}
          key={index}
        />
      );
    });
  };

  handleAddToCard = (shoe) => {
    let index = this.state.gioHang.findIndex((item) => {
      return shoe.id == item.id;
    });
    let cloneGioHang = [...this.state.gioHang];
    if (index == -1) {
      let newSp = { ...shoe, soLuong: 1 };
      cloneGioHang.push(newSp);
    } else {
      cloneGioHang[index].soLuong++;
    }
    this.setState({ gioHang: cloneGioHang });
  };

  handleChangeQuantity = (idShoe, value) => {
    let index = this.state.gioHang.findIndex((item) => {
      return item.id == idShoe;
    });
    if (index == -1) return;
    let cloneGioHang = [...this.state.gioHang];
    cloneGioHang[index].soLuong += value;
    cloneGioHang[index].soLuong == 0 && cloneGioHang.splice(index, 1);
    this.setState({ gioHang: cloneGioHang });
  };

  render() {
    return (
      <div className="container">
        <h2>Shop Shoes</h2>
        <div>
          {this.state.gioHang.length > 0 && (
            <GioHang
              handleChangeQuantity={this.handleChangeQuantity}
              gioHang={this.state.gioHang}
            />
          )}
        </div>
        <div className="row">{this.renderShoes()}</div>
      </div>
    );
  }
}
