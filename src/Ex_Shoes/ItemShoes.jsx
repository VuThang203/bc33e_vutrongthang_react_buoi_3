import React, { Component } from "react";

export default class ItemShoes extends Component {
  render() {
    const { data, handleAddToCard } = this.props;
    return (
      <div className="col-4 mt-5">
        <div className="card h-100">
          <img className="card-img-top" src={data.image} alt="" />
          <div className="card-body">
            <h4 className="card-title">{data.name}</h4>
            <p className="card-text">{data.price}$</p>
            <button
              onClick={() => {
                handleAddToCard(data);
              }}
              className="btn btn-dark"
            >
              Add To Card <i className="fa fa-cart-plus"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
