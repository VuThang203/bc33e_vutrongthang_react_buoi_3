import React, { Component } from "react";
import ItemPhone from "./ItemPhone";
export default class ListPhone extends Component {
  renderListPhone = () => {
    return this.props.phoneData.map((item, index) => {
      return (
        <ItemPhone
          handleChangeCart={this.props.handleChangeCart}
          handleDetail={this.props.handleDetail}
          dataItem={item}
          key={index}
        />
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.renderListPhone()}</div>
      </div>
    );
  }
}
