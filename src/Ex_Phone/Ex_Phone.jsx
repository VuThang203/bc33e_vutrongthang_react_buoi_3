import React, { Component } from "react";
import { dataPhone } from "./data_phone.js";
import ListPhone from "./ListPhone.jsx";
import DetailPhone from "./DetailPhone";
import ModalCart from "./ModalCart";
export default class Ex_Phone extends Component {
  state = {
    phones: dataPhone,
    product: [],
  };

  handleDetail = (productDetail) => {
    this.setState({
      product: productDetail,
    });
  };

  handleChangeCart = (phone) => {
    let index = this.state.product.findIndex((item) => {
      return item.maSP == phone.maSP;
    });
    let cloneProduct = [...this.state.product];
    if (index == -1) {
      let newProduct = { ...phone, soLuong: 1 };
      cloneProduct.push(newProduct);
    } else {
      cloneProduct[index].soLuong++;
    }
    this.setState({ product: cloneProduct });
  };

  handleChangeQuantity = (idPhone, value) => {
    let index = this.state.product.findIndex((item) => {
      return item.maSP == idPhone;
    });
    let cloneProduct = [...this.state.product];
    if (index == -1) return;
    cloneProduct[index].soLuong += value;

    cloneProduct[index].soLuong == 0 && cloneProduct.splice(index, 1);
    this.setState({ product: cloneProduct });
  };

  render() {
    return (
      <div>
        <ListPhone
          handleChangeCart={this.handleChangeCart}
          handleDetail={this.handleDetail}
          phoneData={this.state.phones}
        />
        <DetailPhone phoneDetail={this.state.product} />
        <ModalCart
          handleChangeQuantity={this.handleChangeQuantity}
          cartDetail={this.state.product}
        />
      </div>
    );
  }
}
