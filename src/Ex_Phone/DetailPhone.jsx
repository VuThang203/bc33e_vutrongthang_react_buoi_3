import React, { Component } from "react";

export default class DetailPhone extends Component {
  render() {
    const { phoneDetail } = this.props;
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title ">Chi tiết sản phẩm</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <div className="row">
                <div className="col-4">
                  <h4>{phoneDetail.tenSP}</h4>
                  <img width={150} src={phoneDetail.hinhAnh} alt="" />
                </div>
                <div className="col-8">
                  <h4>Thông số kỹ thuật</h4>
                  <table className="text-left table">
                    <tr>
                      <td>Màn hình</td>
                      <td>{phoneDetail.manHinh}</td>
                    </tr>
                    <tr>
                      <td>Hệ điều hành</td>
                      <td>{phoneDetail.heDieuHanh}</td>
                    </tr>
                    <tr>
                      <td>Camera trước</td>
                      <td>{phoneDetail.cameraTruoc}</td>
                    </tr>
                    <tr>
                      <td>Camera sau</td>
                      <td>{phoneDetail.cameraSau}</td>
                    </tr>
                    <tr>
                      <td>RAM</td>
                      <td>{phoneDetail.ram}</td>
                    </tr>
                    <tr>
                      <td>ROM</td>
                      <td>{phoneDetail.rom}</td>
                    </tr>
                  </table>
                </div>
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-primary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
