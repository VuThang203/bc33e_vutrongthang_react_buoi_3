import React, { Component } from "react";

export default class ItemPhone extends Component {
  render() {
    const { dataItem, handleDetail, handleChangeCart } = this.props;
    return (
      <div className="col-4">
        <div className="card h-100">
          <img className="card-img-top" src={dataItem.hinhAnh} />
          <div className="card-body">
            <h4 className="card-title">{dataItem.tenSP}</h4>
            <p className="card-text">{dataItem.giaBan} VND</p>
            <button
              type="button"
              data-toggle="modal"
              data-target="#modelId"
              className="btn btn-success mr-2"
              onClick={() => {
                handleDetail(dataItem);
              }}
            >
              Xem chi tiết
            </button>
            <button
              type="button"
              data-toggle="modal"
              data-target="#modeCart"
              className="btn btn-danger"
              onClick={() => {
                handleChangeCart(dataItem);
              }}
            >
              Thêm giỏ hàng
            </button>
          </div>
        </div>
      </div>
    );
  }
}
