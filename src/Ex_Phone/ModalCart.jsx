import React, { Component } from "react";

export default class ModalCart extends Component {
  renderTbody = () => {
    return this.props.cartDetail.map((item, index) => {
      return (
        <tr key={index}>
          <td scope="row">{item.maSP}</td>
          <td>
            <img width={100} src={item.hinhAnh} alt="" />
          </td>
          <td>{item.tenSP}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.maSP, 1);
              }}
              className="btn btn-danger"
            >
              +
            </button>
            <span className="px-3">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.maSP, -1);
              }}
              className="btn btn-warning"
            >
              -
            </button>
          </td>
          <td>{item.giaBan * item.soLuong} VND</td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <div
          className="modal fade"
          id="modeCart"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="modelTitleId"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-xl" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">Giỏ hàng</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <table className="table">
                  <thead>
                    <tr>
                      <th>Mã sản phẩm</th>
                      <th>Hình ảnh</th>
                      <th>Tên sản phẩm</th>
                      <th>Số lượng</th>
                      <th>Thành tiền</th>
                    </tr>
                  </thead>
                  <tbody>{this.renderTbody()}</tbody>
                </table>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
